# consoleds

This project is a working FRC driver station created with the `ds-rs` library. It allows for full control of an FRC robot, from autonomous, to teleoperated, and even with multiple joysticks. 

# Notes
* Due to the TUI library used, this application is, and will remain to be, *nix only.
* Joysticks cannot be reordered as with the official NI driver station. Instead, the order of the joysticks is determined by the order they are plugged into the computer.
* This driver station is compatible with the 2018 FRC protocol. Due to the fact that the protocol is proprietary, adding support for the 2019 protocol may be difficult if drastic changes are made.

# Installation and Usage
1. Download the latest release,  or compile this repository with a Rust 2018 edition compiler
2. Run the program like this: `consoleds -t TEAM -c <red/blue> -p <1-3>`

# Known Issues
Driver station crashes on macOS due to the joystick input library used (Redrield/consoleds#1)